// 2. Use the count operator to count the total number of fruits on sale.
db.fruits.aggregate(
  [
    {
        $match: { "onSale": true }
    },
    { $group: { "_id": "$name", "total": { $sum: "$stock" } } },
    { $sort: {"total": -1 } }
  ]
);
// 3. Use the count operator to count the total number of fruits with stock more than 20.
db.fruits.aggregate(
  [
    {
      $match: {
        "stock": {
          $gte: 20
        }
      }
    },
    {
      $count: "fruits_above_20_stock"
    }
  ]
);
// 4. Use the average operator to get the average price of fruits onSale per supplier.
db.fruits.aggregate(
  [
    {
      $match: {
        "onSale": true
      }
    },
    { $group: 
    	{ "_id": "$supplier_id", "averagePrice": { $avg: "$price" } }
    },
    { $sort: {"averagePrice": -1 } }
  ]
);
// 5. Use the max operator to get the highest price of a fruit per supplier.
db.fruits.aggregate(
  [
    {
      $match: {
        "onSale": true
      }
    },
    { $group: 
    	{ "_id": "$supplier_id", "highestPrice": { $max: "$price" } }
    },
    { $sort: {"highestPrice": -1 } }
  ]
);
// 6. Use the min operator to get the lowest price of a fruit per supplier.
db.fruits.aggregate(
  [
    {
      $match: {
        "onSale": true
      }
    },
    { $group: 
    	{ "_id": "$supplier_id", "lowestPrice": { $min: "$price" } }
    },
    { $sort: {"lowestPrice": -1 } }
  ]
);